import React, {Component} from 'react';
import Registration from './Registration.jsx';
import Login from './Login.jsx';
import SocialLogin from './SocialLogin.jsx';
import Reset from './Reset.jsx';
import Forgot from './Forgot.jsx';
import Verification from './Verification.jsx';

class Form extends Component {
  
  constructor(props){
    super(props);
    this.state = {
    };
  }

  render(){    
    return (
      <div>
          {this.props.action.register && <Registration LRObject = {this.props.LRObject}/> }
          {this.props.action.login && <Login LRObject = {this.props.LRObject}/> }
          {this.props.action.socialLogin && <SocialLogin LRObject = {this.props.LRObject}/> }
          {this.props.action.resetPw && <Reset LRObject = {this.props.LRObject}/> }
          {this.props.action.forgotPw && <Forgot LRObject = {this.props.LRObject}/> }
          {this.props.action.verifyEmail && <Verification LRObject = {this.props.LRObject} />}
      </div>
    );
  }
}
export default Form;