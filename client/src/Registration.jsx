import React, {Component} from 'react';

class Registration extends Component {
  
  constructor(props){
    super(props);
    this.state = {
    };
  }

  register = () => {
    let registrationOptions = {};
		registrationOptions.onSuccess = (response) => {
      alert("Confirmation email has been sent.");
			console.log(response);
		};
		registrationOptions.onError = (errors) => {
      alert("Something went wrong, check console");
			console.log(errors);
		};
		registrationOptions.container = "registration-container";

    this.props.LRObject.$hooks.call('customizeFormPlaceholder',{    
      "emailid" : "Enter your email address",
      "password" : "Enter Your password"
    });
    this.props.LRObject.init("registration",registrationOptions);
  }
  
  componentDidMount() {
    this.register();
  }

  componentDidUpdate() {
    this.register();
  }
  
  render(){
    return (
      <div id="registration-container">
      </div>
    );
  }
}
export default Registration;