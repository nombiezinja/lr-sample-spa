import React, { Component } from 'react';
import Form from './Form.jsx';
import Menu from './Menu.jsx';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';


class App extends Component {
  
  initLRObject =  (sott) => {
    var commonOptions = {
      apiKey : process.env.API_KEY,
      appName : process.env.APP_NAME,
      hashTemplate:true,
      resetPasswordUrl: process.env.BASE_URL + '/reset',
      verificationUrl: process.env.BASE_URL + '/confirm',
      sott
    };

    this.setState({
      LRObject: new LoginRadiusV2(commonOptions)
    });
  }
  
  async componentWillMount() {
    try {
      const sott = await fetch(`https://api.loginradius.com/identity/v2/manage/account/sott?apikey=${process.env.API_KEY}&apisecret=${process.env.API_SECRET}`);
      if (!sott.ok) {
        alert(response.statusText);
      }
      const json = await sott.json();
      this.initLRObject(json.Sott);
      console.log(json.Sott);
      this.checkParams();
    } catch (error) {
      console.log(error);
    }
  }
  
  checkParams = () => {
    const params = queryString.parse(this.props.location.search)
    switch (params.vtype ) {
      case 'reset':
        this.handleReset()
        break;
      case 'emailverification':
        this.handleVerify()
        break;
      default:
        break;
    }
  }
  
  handleReset = (token) => {
    this.setState({
      menuVisible: false, 
      formVisible: true,
      action: {resetPw: true} 
    })
  }
 
  handleVerify = (token) => {
    this.setState({
      menuVisible: false, 
      formVisible: true,
      action: {verifyEmail: true}, 
    })
  }

  handleClickHome = () => {
    this.setState({
      action: null, 
      formVisible: false,
      menuVisible: true
    });
    this.props.history.push('/');
  }

  handleClickMenu = () => {
    this.setState({
      formVisible: true,
      menuVisible: false
    });
  }

  handleClickAction = (value) => {
    this.setState({
      action: value
    });
  }

  constructor(props){
    super(props);
    this.state = {
      formVisible: false, 
      menuVisible: true, 
      action: null, 
    }
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  render() {
    return (
      <div>
        <h3 onClick={this.handleClickHome} >Home</h3>

        <div onClick={this.handleClickMenu} >
          {this.state.menuVisible && < Menu 
            LRObject = {this.state.LRObject}
            handleClickAction = {this.handleClickAction}
            /> }
        </div>

        {this.state.formVisible && <Form 
          action = {this.state.action}
          LRObject = {this.state.LRObject}
        />}
      </div>
    ); 
  }
}

export default withRouter(App);