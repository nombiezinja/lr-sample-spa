require('dotenv').config({
    silent: true
});

const port = process.env.PORT;
const express = require('express');
const app = express();

const http = require('http');
const server = http.createServer(app);

const morgan = require('morgan');

app.use(morgan('dev'));
app.set('view engine', 'ejs');
app.set('views', './public');
app.use(express.static('public'));

app.get('*', (req, res) => {
    res.render('index');
});

server.listen(port, function listening () {
    console.log('Listening on %d', server.address().port);
});

